<nav class="sidebar-header" id="principal">

<div class="main-icon" id = "icone">    
<img src="icone_300.png" class="main-icon" height="90"
 >
 <h4 id = "iconeTitulo">Admin Gestor</h4>

</div>
<div class = "container" id ="divmenu">
<ul class="nav flex-column" >
    <li class="nav-item">
        <a href="">    
        <i class="fas fa-address-book"></i>
        <h4><p>Clientes</p></h4>
        <h6>Visualize, cadastre e edite os<br>clientes do sistema</h6>
        </a>
        
    </li>
    <li class="nav-item">
        <a href="">
        <i class="fa fa-users"></i>
        <h4>Cadastrar menu</h4>
        <h6>Fazer o cadastro de uma nova opção<br>de menu para o sistema</h6>
        </a>
    </li>
    <li class="nav-item">
        <a href="">
        <i class="fas fa-sign-out-alt"></i>
                <h4>Sair</h4>
                <h6>Desconectar-se do sistema...</h6>
        </a>
    </li>
</ul>
</div>
</nav>